Description: add back removed gl_exclude.h
 gl_exclude.h was removed from the upstream tarball, presumably because
 it is regenerated anyway during build. This doesn't work in a build
 chroot, however, as xvfb does not support any OpenGL GLX extensions,
 and has been disabled in Debian for some time by disable-glversion.
 In addition, generation of this file seems highly machine specific.
 .
 Since the exclusions previously shipped by upstream likely define a
 least common denominator (and they are what we've been running with
 for years), simply add them back in.
Origin: upstream tarball, pre-0.66
Forwarded: not-needed

--- /dev/null
+++ b/gl_exclude.h
@@ -0,0 +1,6 @@
+// OpenGL Extension Exclusions - may be modified before building.
+//
+// Generated for Mesa project: www.mesa3d.org, Mesa GLX Indirect
+// OpenGL v1.2 (1.5 Mesa 6.4), using FreeGLUT v20400
+//
+// Exclusions omitted for distribution build.
