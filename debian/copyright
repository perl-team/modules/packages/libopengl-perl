Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: OpenGL
Upstream-Contact: Bob 'grafman' Free <grafman at graphcomp.com>
Source: https://metacpan.org/release/OpenGL
Files-Excluded: glext_procs.h include FreeGLUT
Comment: repacked to exclude non-free header files as well as redundant FreeGLUT

Files: *
Copyright: 1998, 1999, Kenneth Albanowski
 2000, Ilya Zakharevich
 2007, 2015, Bob Free
 2009-2016, Christopher Marshall
License: Artistic or GPL-1+

Files: ppport.h
Copyright: 1999, Kenneth Albanowski
 2001, Paul Marquess
 2004-2010, Marcus Holland-Moritz
License: Artistic or GPL-1+

Files: OpenGL.pm OpenGL.xs
Copyright: 1995, Stan Melax
 1998, 1999 Kenneth Albanowski
 2007, Bob Free
 2009, Christopher Marshall
License: Artistic or GPL-1+

Files: glext_consts.h glext_types.h
Copyright: 2013-2016 The Khronos Group Inc.
License: Expat

Files: examples/*
Copyright: 1995, Stan Melax
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2001, Paolo Molaro <lupus@debian.org>
 2002, Ben Burton <benb@acm.org>
 2003, Andrew Pollock <apollock@debian.org>
 2004, Andreas Barth <aba@not.so.argh.org>
 2004, Stephen Zander <gibreel@debian.org>
 2005, Frank Lichtenheld <djpig@debian.org>
 2006-2007, Niko Tyni <ntyni@debian.org>
 2006, Alexis Sukrieh <sukria@debian.org>
 2006, David Moreno Garza <damog@debian.org>
 2006, Gunnar Wolf <gwolf@debian.org>
 2006, Margarita Manterola <marga@debian.org>
 2006-2023, gregor herrmann <gregoa@debian.org>
 2008, Damyan Ivanov <dmn@debian.org>
 2009, 2010, Maximilian Gass <mxey@cloudconnected.org>
 2010, Chris Butler <chrisb@debian.org>
 2012-2019, Florian Schlichting <fsfs@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License
 can be found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the
 General Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and/or associated documentation files (the
 "Materials"), to deal in the Materials without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Materials, and to
 permit persons to whom the Materials are furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Materials.
 .
 THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.
